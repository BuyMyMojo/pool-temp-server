# Pool Temp Server

I'm making a home made pool temp monitoring device that logs temps and also turns off the pool heater at night. I'm making this basic server in Go so I can log the temp to a CSV file to make fancy graphs and other useless stuff!


## API Reference

#### Log temp

```http
  GET /log/${temp}
```

| Parameter | Type     | Description                |
| :-------- | :------- | :------------------------- |
| `temp` | `string` | The current temp to log |

#### List all entries

```http
  GET /list/
```
This just displays a table of all entries in the CSV

![Example of /list/](https://files.buymymojo.net/fxhs9lcwtxoz.png)


## Acknowledgements

- [mux](https://github.com/gorilla/mux)