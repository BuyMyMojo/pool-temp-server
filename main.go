package main

import (
	"encoding/csv"
	"errors"
	"fmt"
	"github.com/gorilla/mux"
	"log"
	"net/http"
	"os"
	"time"
)

// return csv file content
func getAllTemps() (string, error) {
	f, err := os.Open("temps.csv")
	if err != nil {
		return "", err
	}
	defer f.Close()

	r := csv.NewReader(f)
	records, err := r.ReadAll()
	if err != nil {
		return "", err
	}

	htmlResponse := "<html><head><style>table, th, td {\n  border: 1px solid black;\n}</style></head><body><table><tr><th>Date</th><th>Temperature</th></tr>"

	for _, record := range records[1:] {
		htmlResponse += "<tr><td>" + record[0] + "</td><td>" + record[1] + "</td></tr>"
	}

	htmlResponse += "</table></body></html>"

	return htmlResponse, nil
}

func logTemp(temp string) {
	// Get current date and time
	dt := time.Now()

	// Print dt and temp to console
	tempStr := dt.Format("01-02-2006 15:04:05") + "," + temp
	fmt.Println(tempStr)

	// Open CSV
	f, err := os.OpenFile("temps.csv", os.O_WRONLY|os.O_CREATE|os.O_APPEND, 0644)
	if err != nil {

		log.Fatal(err)
	}

	// Create CSV Writer
	w := csv.NewWriter(f)
	defer w.Flush()

	// Create new record
	csvTitles := []string{dt.Format("01-02-2006 15:04:05"), temp}

	// Append record to CSV
	if err := w.Write(csvTitles); err != nil {
		log.Fatalln("error writing record to file", err)
	}
}

func handleTemp(w http.ResponseWriter, r *http.Request) {
	// read temp from request
	vars := mux.Vars(r)

	// reply with 200 OK
	w.WriteHeader(http.StatusOK)

	// log temp
	logTemp(vars["temp"])
}

func listTemps(w http.ResponseWriter, r *http.Request) {
	// reply with 200 OK
	w.WriteHeader(http.StatusOK)
	html, _ := getAllTemps()
	fmt.Fprintf(w, html)
}

func handleRequests() {
	// create a new instance of a mux router
	r := mux.NewRouter()

	// treat anything after /log/ as the temp variable and call handleTemp
	r.HandleFunc("/log/{temp}", handleTemp)

	// treat anything after /list/ as the temp variable and call handleTemp
	r.HandleFunc("/list/", listTemps)

	// Print server link
	fmt.Println("Server started on: http://localhost:8080")

	// Start server
	http.ListenAndServe(":8080", r)
}

func main() {
	// if the CSV does not exist, create it
	if _, err := os.Stat("./temps.csv"); errors.Is(err, os.ErrNotExist) {
		// path/to/whatever does not exist

		// Create CSV
		f, err := os.Create("temps.csv")
		defer f.Close()
		if err != nil {

			log.Fatalln("failed to open file", err)
		}

		// Create CSV Writer
		w := csv.NewWriter(f)
		defer w.Flush()

		// Create CSV Header
		csvTitles := []string{"Time", "Temp"}

		// Write CSV Header
		if err := w.Write(csvTitles); err != nil {
			log.Fatalln("error writing record to file", err)
		}

		// Close CSV Writer
		w.Flush()
	}

	// Start server
	handleRequests()
}
